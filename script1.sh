#!/bin/bash
# Candidato: Italo Batista Queiroz

# Foi utilizado o SO Ubuntu 18
sudo apt install git
cd $HOME
#Criar o clone aqui em HOME
#git clone https://gitlab.com/italobqueiroz/VLG.git

#  git config --global user.email "you@example.com"
#  git config --global user.name "Your Name"

#sudo apt-get update

## Instalando python3.8
sudo apt --fix-broken install
sudo apt-get install build-essential checkinstall
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev

cd /opt/
sudo wget https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tgz
sudo tar xzf Python-3.8.2.tgz
cd Python-3.8.2
sudo ./configure --enable-optimizations
sudo make altinstall

cd /opt
sudo rm -f Python-3.8.2.tgz
## fim instalação do python



## Instalação pip e pipenv
sudo apt install python3-pip
sudo pip3 install pipenv
cd $HOME
cd VLG/venv

pipenv install

# Para ativar o ambiente (deve estar dentro da pasta do ambiente)
# pipenv shell


# Para desativar > deactivate, e depois > exit
######################################

######################################
### Instalando PostgreSQL
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-client-12 postgresql-contrib-9.6 pgadmin4

sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'postgres';"


### Criar e instanciar banco de dados

database='vlgdb'

function ChecarBanco () {
    echo "Checando banco"
    if psql -U postgres -h localhost -lt | cut -d \| -f 1 | grep -w $database; then
        echo "O banco de dados existe!"
    else
        echo "O banco não existe!"
        echo "Criando o banco de dados $database"
        psql -U postgres -h localhost -c "CREATE DATABASE $database"
    fi
}

function ChecarStatusPostgresql () {
    if /etc/init.d/postgresql status | grep -w 'active'; then
        echo "O banco de dados está ativo!"
        ChecarBanco
    else
        echo "O banco de dados está inativo!"
        echo "Iniciando o serviço postgresql"
        /etc/init.d/postgresql start
        ChecarStatusPostgresql
    fi
}

ChecarStatusPostgresql

######################################
### Crontab
sudo apt-get install cron
#crontab -e
# Escolher editor


#Colocar o arquivo python em modo de execução
# chmod u+x cron_script2.sh
crontab -l | { cat; echo "30 23 * * * $HOME/VLG/cron_script2.sh > $HOME/VLG/log_crontab.txt 2>&1"; } | crontab -






