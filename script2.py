import psycopg2
import requests
import csv
import urllib
import pandas as pd
from sqlalchemy import create_engine
from io import StringIO
from datetime import datetime, timedelta
from bs4 import BeautifulSoup

# Candidato: Italo Batista Queiroz

DATA_BASE_URL = "http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/"
DICT_DADOS_URL = 'http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/META/meta_inf_diario_fi.txt'

NOME_TABELA = 'fundos'

def criar_tabela (cur, conn, table_name):
    comando = (
        """
        CREATE TABLE {} (
            CNPJ_FUNDO VARCHAR(20),
            DT_COMPTC DATE,
            VL_TOTAL NUMERIC(17,2),
            VL_QUOTA NUMERIC(27,12),
            VL_PATRIM_LIQ NUMERIC(17,2),
            CAPTC_DIA NUMERIC(14,2),
            RESG_DIA NUMERIC(17,2),
            NR_COTST INTEGER,
            primary key (CNPJ_FUNDO, DT_COMPTC)
        );
        """)

    #comando = "CREATE TABLE {} (CNPJ_FUNDO VARCHAR(20), DT_COMPTC DATE);"

    print(comando.format(table_name))

    try:
        cur.execute(comando.format(table_name))
        return True
    except Exception as e:
        print(type(e))
        print(e)
        return False
        

def checar_tabela (cur, table_name):
    # Checar se a tabela existe e tem dados salvos

    print('Checando tabela: {}'.format(table_name))

    try:
        cur.execute("SELECT * FROM {} LIMIT 1;".format(table_name))
        list_usuarios = cur.fetchall()
        return (True, len(list_usuarios))
    except psycopg2.errors.UndefinedTable as e:
        return (False, -1)

def inserir_linha(cur, linha, table_name):
    comando = ("""
        INSERT INTO {} (CNPJ_FUNDO, DT_COMPTC, VL_TOTAL, VL_QUOTA, VL_PATRIM_LIQ, CAPTC_DIA, RESG_DIA, NR_COTST)
        VALUES ('{}', '{}', {}, {}, {}, {}, {}, {})
        """)

    try:
        # print(comando.format(table_name, linha[0], linha[1], linha[2], linha[3], linha[4], linha[5], linha[6], linha[7]))
        cur.execute(comando.format(table_name, linha[0], linha[1], linha[2], linha[3], linha[4], linha[5], linha[6], linha[7]))
    except Exception as e:
        print("Erro: {}", format(e))
        print("Comando: {}".format(comando.format(table_name, linha[0], linha[1], linha[2], linha[3], linha[4], linha[5], linha[6], linha[7])))
    

def inserir_tabela2(cur, name_file):
    url_file = DATA_BASE_URL + name_file

    print('Download de dados...')
    print('URL: {}'.format(url_file))
    with requests.Session() as s:
        download = s.get(url_file)

        decoded_content = download.content.decode('utf-8')

        reader = csv.reader(decoded_content.splitlines(), delimiter = ';')
        data_list = list(reader)
        print('Numero de linhas: {}'.format(len(data_list)))

        print('Inserindo linhas...')
        start = datetime.now()
        for x in range(1, len(data_list)):
            inserir_linha(cur, data_list[x], NOME_TABELA)
        end = datetime.now()
        tempo = end - start
        print('Tempo de execução: {}'.format(tempo))

def inserir_tabela_rapido(cur, name_file):
    url_file = DATA_BASE_URL + name_file

    file_text = urllib.request.urlopen(url_file)

    file_buf = StringIO(file_text.read().decode('utf-8'))
    file_buf.readline()

    start = datetime.now()
    try:
        cur.copy_from(file_buf, NOME_TABELA, ';')
    except Exception as e:
        print(e)
    end = datetime.now()
    tempo = end - start
    print('Tempo de execução: {}'.format(tempo))
    file_buf.close()
        
        

    
def captar_toda_base(cur):
    print('Captando base de dados...')
    base_page = requests.get(DATA_BASE_URL)
    soup  = BeautifulSoup(base_page.text, 'html.parser')
    list_a = soup.find_all('a')

    list_csv = []

    for x in list_a:
        if ('.csv' in x.get('href')):
            list_csv.append(x.get('href'))



    for x in list_csv:
        print('Inserindo tabela {}'.format(x))
        inserir_tabela_rapido(cur, x)

def checar_colunas(today_date_df, table_name):

    colunas = today_date_df.columns.tolist()

    



    engine = create_engine('postgresql://postgres:postgres@localhost:5432/testedb')

    try:
        conn = engine.connect()
    except Exception as e:
        print('Não foi possível conectar ao banco')
        return False

    db_columns = pd.read_sql_query("SELECT * FROM {} LIMIT 1".format(table_name), conn).columns

    new_columns = list(filter(lambda x: x not in db_columns, today_date_df.columns))

    if(len(new_columns) > 0):
        print('-- Novas colunas --')
        for coluna in new_columns:
            print('Coluna: {}'.format(coluna))
            comando = "ALTER TABLE {} ADD COLUMN {} {};".format(table_name, coluna, 'text')
            print(comando)
            conn.execute(comando)
    else:
        print('Não há novas colunas')
    
    
def captar_dados_diarios(table_name):

    

    hoje = datetime.now()

    hoje_ano = hoje.year
    hoje_mes = hoje.month
    hoje_dia = hoje.day
    print('Data: {}/{}/{}'.format(hoje_dia, hoje_mes, hoje_ano))
    print('Hora: {:02d}:{:02d}'.format(hoje.hour, hoje.minute))

    # hoje_dia = 8
    # hoje_mes = 4
    # hoje_ano = 2020

    print("Data simulado: " + "{}-{}-{}".format(hoje_ano, str(hoje_mes).zfill(2), str(hoje_dia).zfill(2)))

    base_name = 'inf_diario_fi_'
    url_file = DATA_BASE_URL + base_name + str(hoje_ano) + str(hoje_mes).zfill(2) + '.csv'
    print("URL: " + url_file)


    try:
        file_text = urllib.request.urlopen(url_file)
    except Exception as e:
        print('URL não encontrada')
        print(e)
        return False
        
    print('Passou do break')
    file_buf = StringIO(file_text.read().decode('utf-8'))
    df = pd.read_csv(file_buf, sep=';')
    file_buf.close()

    
    today_date_df = df.loc[df['DT_COMPTC'] == "{}-{}-{}".format(hoje_ano, str(hoje_mes).zfill(2), str(hoje_dia).zfill(2))]
    

    today_date_df.columns = map(str.lower, today_date_df.columns)



    colunas = today_date_df.columns.tolist()

    ## Adicionar uma coluna para teste
    # today_date_df.insert(len(colunas) ,'numero', 41.5)
    # print('\n-DEPOIS')
    # print(today_date_df.head())
    
    checar_colunas(today_date_df, table_name)
    # print(colunas[0])

    engine = create_engine('postgresql://postgres:postgres@localhost:5432/testedb')

    try:
        engine.connect()
    except Exception as e:
        print('Não foi possível conectar ao banco')
        return False

    print('Quantidade de linhas a serem escritas: {}'.format(today_date_df.shape[0]))

    if(today_date_df.shape[0] < 1):
        print('Não há linhas a serem escritas')
    else:
        try:
            today_date_df.to_sql(table_name, con=engine, if_exists='append', index=False)
            print('Concluído!')
        except Exception as e:
            print('Erro ao inserir linhas')
            print(type(e))
            print(e)
            
    


def main():

    dbname = 'vlgdb'

    try:
        conn = psycopg2.connect("dbname={} user=postgres password=postgres host=localhost port=5432".format(dbname))
        conn.autocommit = True
        cur = conn.cursor()
    except psycopg2.Error as e:
        print('Não foi possível conectar no banco de dados')
        return False
    
    

    resposta = checar_tabela(cur, NOME_TABELA)
    if(resposta[0]):
        print('Tabela "{}" existe'.format(NOME_TABELA))
        if(resposta[1] == 0):
            print('Tabela "{}" está vazia'.format(NOME_TABELA))
            print('Captando toda a base')
            captar_toda_base(cur)
        else:
            print('Captando dados diários')
            captar_dados_diarios(NOME_TABELA)
            
    else:
        print('Tabela "{}" não existe'.format(NOME_TABELA))
        if(criar_tabela(cur, conn, NOME_TABELA)):
            print('Tabela criada com sucesso')
            captar_toda_base(cur)
        else:
            print('Não foi possível criar tabela')
    

    cur.close()
    conn.close()



if __name__ == "__main__":
    main()
